class Name:
    def __init__(self, name='B'):
        self.__name = name

    @property
    def name(self):
        return f'The name is {self.__name}'

    @name.setter
    def name(self, name):
        if name == "":
            self.__name = 'defailt name'
        else:
            self.__name = name

    @name.deleter
    def name(self):
        del self.__name


o1 = Name()
print(o1.name)
o2 = Name('Bob')
print(o2.name)
o2.name = 'Anna'
print(o2.name)
o2.name = ''
print(o2.name)


